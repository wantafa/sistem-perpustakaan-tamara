-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 13, 2022 at 01:45 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tama`
--

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `id` int(11) UNSIGNED NOT NULL,
  `judul` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penerbit` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pengarang` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun_terbit` date DEFAULT NULL,
  `deskripsi` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jml_buku` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lokasi` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`id`, `judul`, `penerbit`, `pengarang`, `tahun_terbit`, `deskripsi`, `jml_buku`, `lokasi`, `foto`, `created_at`, `updated_at`) VALUES
(4, 'Minima duis fuga Re', 'Sequi sed veritatis', 'Voluptas quod evenie', '1987-06-05', 'Incidunt ullamco et', '4', 'Aliquip ratione elig', 'uploads/foto/1658975307_buku.jpg', '2022-07-14 04:36:16', '2022-08-23 13:12:41'),
(5, 'Quaerat illum odit', 'Duis et voluptatem m', 'In eos id quisquam', '2006-04-18', 'Voluptatem asperior', '56', 'Dolor beatae aut rep', 'uploads/foto/1658975320_buku.jpg', '2022-07-15 03:44:31', '2022-07-28 02:28:40'),
(6, 'Sit est repudiandae', 'Nostrud aperiam omni', 'Consectetur dolore', '2016-11-16', 'Nisi sed rerum qui u', '35', 'Alias tempora pariat', 'uploads/foto/1658975332_buku.jpg', '2022-07-15 03:47:24', '2022-07-28 02:28:52');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_10_22_143924_create_notes_table', 2),
(5, '2020_10_22_144046_create_subjects_table', 2),
(6, '2020_11_23_124053_add_column_to_users_table', 3),
(7, '2020_11_23_144355_create_post_table', 4),
(8, '2021_05_23_105316_create_perpustakaan_table', 5),
(9, '2021_05_23_105820_create_arsip_table', 5),
(10, '2021_05_23_114513_create_profil_table', 5),
(11, '2021_05_23_114754_create_renstra_table', 5),
(12, '2021_05_23_114854_create_renkerta_table', 5),
(13, '2021_05_23_115031_create_lakip_table', 5),
(14, '2021_05_23_115307_create_pengadaan_table', 5),
(15, '2021_09_18_141545_create_kontak_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `buku_id` int(11) DEFAULT NULL,
  `kd_pinjam` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_pinjam` date DEFAULT NULL,
  `tgl_kembali` date DEFAULT NULL,
  `tgl_kmbl_real` date DEFAULT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `denda` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `user_id`, `buku_id`, `kd_pinjam`, `tgl_pinjam`, `tgl_kembali`, `tgl_kmbl_real`, `status`, `denda`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 'PM/150722/252', '2022-12-21', '2022-10-22', '2022-07-18', 'Pinjam', '555555', 'Repudiandae do odit', '2022-07-15 03:52:11', '2022-07-16 07:06:02'),
(2, 2, 5, 'PM/150722/252', '2022-12-21', '2022-08-22', '2022-08-28', 'Pinjam', '2000', 'Repudiandae do odit', '2022-07-15 03:52:48', '2022-08-28 02:49:17'),
(3, 1, 4, 'PM/230822/600', '2022-08-23', '2022-08-25', NULL, 'Kembali', NULL, 'Pinjam', '2022-08-23 13:04:26', '2022-08-23 13:12:41');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '/uploads/images/profile/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `role`, `image`, `email_verified_at`, `password`, `remember_token`, `last_login`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'dzakwan', 'admin@admin.com', 'admin', '/uploads/images/profile/Admin_1658975068.png', NULL, '$2y$10$8eBuhFN5vlgPdtQl0VL9z.x5ozUT.rqdw0nE8Dw7rduumEb/rk6de', NULL, '2022-09-05 21:06:03', '2020-10-01 21:26:07', '2022-09-05 14:06:03'),
(2, 'Shinta', 'shinta', 'shinta@admin.com', 'admin', '/uploads/images/profile/default.png', NULL, '$2y$10$S8eoSneEkERDCnOlEMl1IeCaZMw3V4HF6W/upRsO8F.mySAoGp/Py', NULL, NULL, '2020-10-04 06:40:03', '2021-09-09 01:23:59'),
(3, 'Riski', 'riski', 'riski@admin.com', 'siswa', '/uploads/images/profile/default.png', NULL, '$2y$10$8/yhNvxSAF6PJcxe4R4y3OJw6IFbpFpXtrptO.Sw9yj6lVP/3CUUW', NULL, NULL, '2020-11-24 07:50:11', '2021-09-09 01:24:40'),
(4, 'Tamara', 'tamara', 'tamara@siswa.com', 'siswa', '/uploads/images/profile/default.png', NULL, '$2y$10$sa71K3QNRAfRwL8jTI80VOos.Opzos8uEcAiRDt5KBa9u.4B7H8I6', NULL, '2022-08-23 20:23:54', '2022-08-23 13:23:26', '2022-08-23 13:23:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
