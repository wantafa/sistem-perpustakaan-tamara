<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return redirect(route('login'));
});

Route::group(['middleware' => 'auth'], function() {

    // Buku
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    Route::get('/buku', 'BukuController@index')->name('buku');
    Route::post('/buku', 'BukuController@store')->name('store');
    Route::get('/buku/{buku}/edit', 'BukuController@edit')->name('edit');
    Route::patch('/buku/update/{buku}', 'BukuController@update')->name('update');
    Route::get('/buku/show/{buku}', 'BukuController@show')->name('show');
    Route::delete('/buku/{buku}', 'BukuController@destroy')->name('buku.delete');
    
    // Pinjam Buku
    Route::get('/transaksi', 'PinjamBukuController@index')->name('transaksi');
    Route::post('/transaksi', 'PinjamBukuController@store')->name('transaksi.store');
    Route::get('/transaksi/{transaksi}/edit', 'PinjamBukuController@edit')->name('edit');
    Route::patch('/transaksi/update/{transaksi}', 'PinjamBukuController@update')->name('transaksi.update');
    Route::get('/transaksi/show/{transaksi}', 'PinjamBukuController@show')->name('show');
    Route::delete('/transaksi/{transaksi}', 'PinjamBukuController@destroy')->name('transaksi.delete');

    Route::get('/proses/kembali/{transaksi}', 'PinjamBukuController@view_proses');
    Route::patch('/proses/update/{transaksi}', 'PinjamBukuController@proses_update');

    Route::get('/pinjam_buku/laporan', 'PinjamBukuController@cetakForm')->name('cetak');
    Route::get('/pinjam_buku/pinjam/{tglawal}/{tglakhir}', 'PinjamBukuController@cetakpinjam')->name('pinjam');
    Route::get('/pinjam_buku/kembali/{tglawal}/{tglakhir}', 'PinjamBukuController@cetakkembali')->name('kembali');
    
    // Anggota
    Route::get('/anggota', 'UserController@anggota')->name('anggota');

  // Profile
    Route::get('profile', 'Admin\UserController@profile')->name('profile');
    Route::post('profile', 'Admin\UserController@update')->name('updateProfile');
    Route::post('profile/image', 'Admin\UserController@ubahfoto')->name('ubahFoto');
    Route::patch('profile/password', 'Admin\UserController@ubahPassword')->name('ubahPassword');

  // Manajemen User
  Route::get('/manajemen_user', 'UserController@index')->name('manajemen_user.index');
  Route::post('/manajemen_user', 'UserController@store')->name('manajemen_user.store');
  Route::get('/manajemen_user/{manajemen_user}/edit', 'UserController@edit')->name('edit');
  Route::patch('/manajemen_user/update/{manajemen_user}', 'UserController@update')->name('update');
  Route::get('/manajemen_user/show/{manajemen_user}', 'UserController@show')->name('show');
  Route::delete('/manajemen_user/{manajemen_user}', 'UserController@destroy')->name('manajemen_user.delete');
});



