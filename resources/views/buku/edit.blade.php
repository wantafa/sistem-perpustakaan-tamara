<div class="row">
    <div class="col-md-6">
        <div class="form-group">
        <input type="hidden" value="{{ $buku->id }}" id="id_data"/>
            <label>Judul</label>
            <input id="judul" name="judul" placeholder="Masukkan Judul" type="text" class="form-control"
                value="{{$buku->judul}}" required>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Foto</label>
            <img width="200" height="200" src="{{ $buku->foto }}"/>
            <input id="foto" name="foto" type="file"
                class="foto form-control" style="margin-top: 20px;" value="{{$buku->foto}}" required>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Penerbit</label>
            <input id="penerbit" name="penerbit" placeholder="Masukkan Penerbit" type="text"
                class="form-control" value="{{$buku->penerbit}}" required>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Jumlah Buku</label>
            <input id="jml_buku" name="jml_buku" placeholder="Masukkan Jumlah Buku" type="number" min="1"
                class="form-control" value="{{$buku->jml_buku}}" required>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Pengarang</label>
            <input id="pengarang" name="pengarang" placeholder="Masukkan Pengarang" type="text"
                class="form-control" value="{{$buku->pengarang}}" required>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Tahun Terbit</label>
            <input id="tahun_terbit" name="tahun_terbit" type="date"
                class="form-control" value="{{$buku->tahun_terbit}}" required>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Deskripsi</label>
            <input id="deskripsi" name="deskripsi" placeholder="Masukkan Deskripsi" type="text"
                class="form-control" value="{{$buku->deskripsi}}" required>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Lokasi</label>
            <input id="lokasi" name="lokasi" placeholder="Masukkan Lokasi" type="text"
                class="form-control" value="{{$buku->lokasi}}" required>
        </div>
    </div>