@extends ('layouts.master')
@section('title', 'Buku')
@section('content')
<div class="content">
</div>

<div class="section-header">
    <h1>Buku</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
      <div class="breadcrumb-item">Buku</div>
    </div>
  </div>

    <section class="content" style="padding-top: 5px">
      <div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
          <button class="btn btn-icon icon-left btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-plus"></i>Tambah Data</button>
            @if (session('status'))
                <div class="">
                    {{ session('status') }}
                </div>
            @endif
    </div>
      </div>

<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4>Buku</h4>
        </div>

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-hover table-striped" id="table-1">
              <thead>                                 
                <tr class="table-info">
                  <th>No</th>
                  <th>Foto</th>
                  <th>Judul</th>
                  <th>Jumlah Buku</th>
                  <th>Penerbit</th>
                  <th>Pengarang</th>
                  <th>Tahun Terbit</th>
                  <th>Deskripsi</th>
                  <th>Lokasi</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>             
                @foreach ($buku as $item)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td><img src="{{$item->foto}}" alt="image" width="50" height="50" /></td>
                  <td>{{ $item->judul}}</td>
                  <td>{{ $item->jml_buku}}</td>
                  <td>{{ $item->penerbit}}</td>
                  <td>{{ $item->pengarang}}</td>
                  <td>{{ $item->tahun_terbit}}</td>
                  <td>{{ $item->deskripsi}}</td>
                  <td>{{ $item->lokasi}}</td>
                  @if (Auth::user()->role == 'admin' || Auth::user()->role == 'petugas')
                  <td>
                  
                  {{-- <a href="#" data-id="{{$item->id}}" class="btn btn-icon btn-success btn-sm btn-show"><i class="far fa-eye"></i></a> --}}
                  <a href="#" data-id="{{$item->id}}" class="btn btn-icon btn-primary btn-sm btn-edit"><i class="far fa-edit"></i></a>
                  <a href="#" data-id="{{$item->id}}" class="btn btn-icon btn-danger btn-sm swal-6 "><i class="fas fa-trash"></i></a>
                  <form action="{{ route('buku.delete', $item->id) }}" id="delete{{ $item->id }}" method="POST">
                    @csrf        
                    @method('delete')
                    </form>
                    </td>
                  @endif
                  </tr>
                @endforeach                    
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
    </section>
  @section('modal')

  {{-- MODAL TAMBAH DATA --}}
  <div class="modal fade" tabindex="-1" role="dialog" id="exampleModal">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Tambah Buku</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form action="{{ route('store') }}" method="POST" enctype="multipart/form-data">
                @csrf
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('foto') class="text-danger" @enderror>Foto @error('foto') | {{ $message }} @enderror</label>
                      <br>
                      <img width="200" height="200"/>
                      <input id="foto" name="foto" type="file" class="upload form-control" value="{{old('foto')}}" style="margin-top: 20px;">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('judul') class="text-danger" @enderror>Judul @error('judul') | {{ $message }} @enderror</label>
                      <input id="judul" name="judul" placeholder="Masukkan Judul" type="text" class="form-control" value="{{old('judul')}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('jml_buku') class="text-danger" @enderror>Jumlah Buku @error('jml_buku') | {{ $message }} @enderror</label>
                      <input id="jml_buku" name="jml_buku" placeholder="Masukkan Jumlah Buku" type="number" min="1" class="form-control" value="{{old('jml_buku')}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('penerbit') class="text-danger" @enderror>Penerbit @error('penerbit') | {{ $message }} @enderror</label>
                      <input id="penerbit" name="penerbit" placeholder="Masukkan Penerbit" type="text" class="form-control" value="{{old('penerbit')}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('pengarang') class="text-danger" @enderror>Pengarang @error('pengarang') | {{ $message }} @enderror</label>
                      <input id="pengarang" name="pengarang" placeholder="Masukkan Pengarang" type="text" class="form-control" value="{{old('pengarang')}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('tahun_terbit') class="text-danger" @enderror>Tahun Terbit @error('tahun_terbit') | {{ $message }} @enderror</label>
                      <input id="tahun_terbit" name="tahun_terbit" type="date" class="form-control" value="{{old('tahun_terbit')}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('deskripsi') class="text-danger" @enderror>Deskripsi @error('deskripsi') | {{ $message }} @enderror</label>
                      <input id="deskripsi" name="deskripsi" placeholder="Masukkan Deskripsi" type="text" class="form-control" value="{{old('deskripsi')}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('lokasi') class="text-danger" @enderror>Lokasi @error('lokasi') | {{ $message }} @enderror</label>
                      <input id="lokasi" name="lokasi" type="text" placeholder="Masukkan Lokasi" class="form-control" value="{{old('lokasi')}}">
                    </div>
                  </div>
            </div>
          </div>
              <div class="modal-footer bg-whitesmoke br">
                  <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
      </div>
  </div>

  {{-- MODAL LIHAT DATA --}}
  <div class="modal fade" tabindex="-1" role="dialog" id="modal-lihat">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Lihat Buku</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form action="{{ route('store') }}" method="POST" id="form-show">
                @csrf
              <div class="modal-body">
                
              </div>
              <div class="modal-footer bg-whitesmoke br">
                  <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                  {{-- <button type="submit" class="btn btn-primary">Simpan</button> --}}
              </div>
            </form>
          </div>
      </div>
  </div>

  {{-- MODAL EDIT DATA --}}
  <div class="modal fade" tabindex="-1" role="dialog" id="modal-edit">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Edit Buku</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form action="{{ route('store') }}" method="POST" id="form-edit" enctype="multipart/form-data">
              @method('PATCH')
                @csrf
              <div class="modal-body">

              </div>
              <div class="modal-footer bg-whitesmoke br">
                  <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                  <button type="button" class="btn btn-primary btn-update">Simpan</button>
              </div>
            </form>
          </div>
      </div>
  </div>
  @endsection

@endsection
@push ('page-scripts')
@include('buku.js.buku-js')
@endpush