<script>
    @if($errors->any())
        $('#exampleModal').modal('show')
    @endif
  
    $(".swal-6").click(function(e) {
      // id = e.target.dataset.id;
      id = $(this).data('id')
    swal({
        title: 'Kamu Yakin Mau Hapus?',
        text: 'Jika di Hapus, Data akan hilang!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
        swal('Data Berhasil diHapus :)', {
          icon: 'success',
        });
        $(`#delete${id}`).submit();
        } else {
        swal('Data Kamu Aman!');
        }
      });
  });
  
        $('.btn-edit').on('click', function() {
          let id = $(this).data('id')
          $.ajax({
            url:`/buku/${id}/edit`,
            method:"GET",
            success: function(data) {
              $('#modal-edit').find('.modal-body').html(data)
              $('#modal-edit').modal('show')

              $(function () {
        $(".foto").change(readURL)
        $("#f").submit(function(){
        // do ajax submit or just classic form submit
        //  alert("fake subminting")
          return false
        });
      });
  
            },
            error:function(error){
              console.log(error)
            }
          })
        })
  

        
        $('.btn-update').on('click', function() {
          let id = $('#form-edit').find('#id_data').val()
          var foto = $('#foto').val()
          var formData = new FormData($('#form-edit')[0]);
  
          // let formData = $('#form-edit').serialize()
          $.ajaxSetup({
                headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
  
          $.ajax({
            type:"POST",
            url:`/buku/update/${id}`,
            data:formData,
            contentType: false,
            processData: false,
            success: function(data) {
              swal('Data Buku Telah diubah :)','', {
                    timer: 2000,
                    icon: 'success',
                });
              // $('#modal-edit').find('.modal-body').html(data)
              $('#modal-edit').modal('hide')
              window.location.assign('/buku')
            },
            error:function(err){
              console.log(err)
            }
          })
        })
  
        $('.btn-show').on('click', function() {
          let id = $(this).data('id')
          $.ajax({
            url:`/buku/show/${id}`,
            method:"GET",
            success: function(data) {
              $('#modal-lihat').find('.modal-body').html(data)
              $('#modal-lihat').modal('show')
            },
            error:function(error){
              console.log(error)
            }
          })
        })

  </script>

  <script>
    function readURL() {
        var input = this;
        if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            $(input).prev().attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[0]);
        }
      }

    $(function () {
            $(".upload").change(readURL)
            $("#f").submit(function(){
                // do ajax submit or just classic form submit
              //  alert("fake subminting")
                return false
            })
        })
  </script>