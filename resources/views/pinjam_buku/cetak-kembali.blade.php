<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Status Pinjam</title>
</head>
<style type="text/css">
    .tg .tg-baqh{text-align:center;vertical-align:middle}
    </style>
<body>
    <div class="form-group">
        <p align="center"><b>Laporan Peminjaman Buku Status Pinjam</b></p>
        <table clas="static" class="tg" rules="all" border="1px" style="width: 40%; margin-left:34px;margin-bottom:10px">
            <tr>
                <th>No</th>
                <th>Nama Buku</th>
                <th>Total Dipinjam</th>
            </tr>
            </thead>
            <tbody>    
                @foreach ($buku_terbanyak as $item)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $item->buku->judul}}</td>
                  <td class="tg-baqh">{{ $item->total}}</td>
                </tr>
                @endforeach
        </table>
    </div>

    <div class="form-group">
        <table clas="static" class="tg" align="center" rules="all" border="1px" style="width: 95%;">
            <tr>
                <th>No</th>
                <th>Kode Pinjam</th>
                <th>Buku</th>
                <th>Peminjam</th>
                <th>Tanggal Pinjam</th>
                <th>Tanggal Kembali</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>    
                @foreach ($kembali as $item)
                <tr>
                  <td class="tg-baqh">{{ $no++ }}</td>
                  <td class="tg-baqh">{{ $item->kd_pinjam}}</td>
                  <td class="tg-baqh">{{ $item->buku->judul}}</td>
                  <td class="tg-baqh">{{ $item->user->name}}</td>
                  <td class="tg-baqh">{{ $item->tgl_pinjam}}</td>
                  <td class="tg-baqh">{{ $item->tgl_kembali}}</td>
                  <td class="tg-baqh">
                      <div class="row">
                    @if ($item->status == 'Pinjam')
                    <label class="badge badge-sm badge-warning">Pinjam</label>
                    @elseif ($item->status == 'Kembali')
                    <label class="badge badge-success">Kembali</label>
                    @endif
                </td>
                </tr>
                @endforeach
        </table>
    </div>
    <script type="text/javascript">
        window.print();
    </script>
</body>
</html>