@extends ('layouts.master')
@section('title', 'Cetak Pinjaman Buku')
@section('content')

<div class="section-header">
    <h1>Cetak Pinjaman Buku</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="/">Dashboard</a></div>
      <div class="breadcrumb-item">Cetak Pinjaman Buku</div>
    </div>
  </div>

    <section class="content" style="padding-top: 5px">
      <div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
            @if (session('status'))
                <div class="">
                    {{ session('status') }}
                </div>
            @endif
    </div>
      </div>

      <div class="row">
        <div class="col-lg-4 col-md-2 col-sm-2 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-primary">
              <i class="fas fa-chart-line"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total Pinjaman Buku</h4>
              </div>
              <div class="card-body">
                {{ $pinjam_buku->count() }}
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-3 col-sm-3 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-warning">
              <i class="fas fa-hourglass-half"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Pinjam</h4>
              </div>
              <div class="card-body">
                {{ $pinjam->count() }}
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-3 col-sm-3 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-success">
              <i class="fas fa-sync"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Kembali</h4>
              </div>
              <div class="card-body">
                {{ $kembali->count() }}
              </div>
            </div>
          </div>
        </div>
        </div>                  

<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4>Cetak Pinjaman Buku</h4>
        </div>
        <div class="card-body">
          <div class="input-group-mb-3">
            <label for="label">Tanggal Awal</label>
            <input type="text" name="tglawal" id="tglawal" class="form-control datepicker border-primary"/> 
          </div> 
          <br>
          <div class="input-group-mb-3">
            <label for="label">Tanggal Akhir</label>
            <input type="text" name="tglakhir" id="tglakhir" class="form-control datepicker1 border-primary"/> 
          </div> 
          <br>
          <div class="row">
            <div class="buttons d-flex mx-auto">
              <a href="" onclick="this.href='/pinjam_buku/pinjam/'+ document.getElementById('tglawal').value + '/' + document.getElementById('tglakhir').value"  target="_blank" class="btn btn-info ">Cetak Pinjam <i class="fa fa-print"></i><span class="badge badge-transparent">{{ $pinjam->count() }}</span></a>
              <a href="" onclick="this.href='/pinjam_buku/kembali/'+ document.getElementById('tglawal').value + '/' + document.getElementById('tglakhir').value"  target="_blank" class="btn btn-primary ">Cetak Kembali <i class="fa fa-print"></i><span class="badge badge-transparent">{{ $kembali->count() }}</span></a>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
      </div>
    </div>
  </div>
    </section>
@endsection
@push ('page-scripts')
@include('pinjam_buku.js.pinjam_buku-js')
@endpush