@extends ('layouts.master')
@section('title', 'Transaksi')
@section('content')
<div class="content">
</div>

<div class="section-header">
    <h1>Transaksi</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
      <div class="breadcrumb-item">Transaksi</div>
    </div>
  </div>

    <section class="content" style="padding-top: 5px">
      <div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
          <button class="btn btn-icon icon-left btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-plus"></i>Tambah</button>
            @if (session('status'))
                <div class="">
                    {{ session('status') }}
                </div>
            @endif
    </div>
      </div>

<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4>Transaksi</h4>
        </div>

        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-hover table-striped" id="table-1">
              <thead>                                 
                <tr class="table-info">
                  <th>No</th>
                  <th>Kode Pinjam</th>
                  <th>Buku</th>
                  <th>Peminjam</th>
                  <th>Tanggal Pinjam</th>
                  <th>Tanggal Kembali</th>
                  <th>Status</th>
                  <th>Tanggal Kembali</th>
                  <th>Denda</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>             
                @foreach ($pinjam_buku as $item)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $item->kd_pinjam}}</td>
                  <td>{{ $item->buku->judul}}</td>
                  <td>{{ $item->user->name}}</td>
                  <td>{{ tgl_ind($item->tgl_pinjam)}}</td>
                  <td>{{ tgl_ind($item->tgl_kembali)}}</td>
                  <td>{{ $item->status}}</td>
                  <td>{{ tgl_ind($item->tgl_kmbl_real)}}</td>
                  <td>@rp($item->denda)</td>
                  @if (Auth::user()->role == 'admin' || Auth::user()->role == 'petugas')
                      
                  <td>
                  @if ($item->status == 'Kembali')
                  
                  @else
                  <a href="#" data-id="{{$item->id}}" class="btn btn-icon btn-info btn-sm btn-proses">Proses</a>
                  @endif
                  {{-- <a href="#" data-id="{{$item->id}}" class="btn btn-icon btn-success btn-sm btn-show"><i class="far fa-eye"></i></a> --}}
                  
                  {{-- <form action="{{ route('pinjam_buku.update', $item->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    <button class="btn btn-icon btn-primary btn-sm" onclick="return confirm('Anda yakin Buku ini sudah kembali?')">Kembali</i>
                    </button>
                  </form> --}}
                  <a href="#" data-id="{{$item->id}}" class="btn btn-icon btn-danger btn-sm swal-6 "><i class="fas fa-trash"></i></a>
                  <form action="{{ route('transaksi.delete', $item->id) }}" id="delete{{ $item->id }}" method="POST">
                    @csrf        
                    @method('delete')
                    </form>
                    </td>
                  @endif
                  </tr>
                @endforeach                    
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
    </section>
  @section('modal')

  {{-- MODAL TAMBAH DATA --}}
  <div class="modal fade" tabindex="-1" role="dialog" id="exampleModal">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Tambah Pinjam Buku</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form action="{{ route('transaksi.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('kd_pinjam') class="text-danger" @enderror>Kode Pinjam @error('kd_pinjam') | {{ $message }} @enderror</label>
                      <input id="kd_pinjam" name="kd_pinjam" value="{{'PM/'.\Carbon\Carbon::now()->format('dmy') .'/'.rand(4, 1000)}}" type="text" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('tgl_pinjam') class="text-danger" @enderror>Tanggal Pinjam @error('tgl_pinjam') | {{ $message }} @enderror</label>
                      <input id="tgl_pinjam" name="tgl_pinjam" placeholder="Masukkan Tanggal Pinjam" type="date" class="form-control" value="{{old('tgl_pinjam')}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('tgl_kembali') class="text-danger" @enderror>Tanggal Kembali @error('tgl_kembali') | {{ $message }} @enderror</label>
                      <input id="tgl_kembali" name="tgl_kembali" placeholder="Masukkan Tanggal Kembali" type="date" class="form-control" value="{{old('tgl_kembali')}}">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('buku_id') class="text-danger" @enderror>Buku @error('buku_id') | {{ $message }} @enderror</label>
                      <select name="buku_id" class="form-control">
                        <option value="">Pilih Buku</option>
                        @foreach ($buku as $item)
                        <option value="{{ $item->id }}">{{ $item->judul }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label @error('keterangan') class="text-danger" @enderror>Keterangan @error('keterangan') | {{ $message }} @enderror</label>
                      <input id="keterangan" name="keterangan" type="text" placeholder="Masukkan Keterangan" class="form-control" value="{{old('keterangan')}}">
                    </div>
                  </div>
            </div>
          </div>
              <div class="modal-footer bg-whitesmoke br">
                  <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                  <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
      </div>
  </div>

  {{-- MODAL LIHAT DATA --}}
  <div class="modal fade" tabindex="-1" role="dialog" id="modal-lihat">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Lihat Pinjam Buku</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form action="{{ route('store') }}" method="POST" id="form-show">
                @csrf
              <div class="modal-body">
                
              </div>
              <div class="modal-footer bg-whitesmoke br">
                  <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                  {{-- <button type="submit" class="btn btn-primary">Simpan</button> --}}
              </div>
            </form>
          </div>
      </div>
  </div>

  {{-- MODAL EDIT DATA --}}
  <div class="modal fade" tabindex="-1" role="dialog" id="modal-proses">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Proses Transaksi</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form action="{{ route('store') }}" method="POST" id="form-proses" enctype="multipart/form-data">
              @method('PATCH')
                @csrf
              <div class="modal-body">

              </div>
              <div class="modal-footer bg-whitesmoke br">
                  <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                  <button type="button" class="btn btn-primary btn-proses-up">Simpan</button>
              </div>
            </form>
          </div>
      </div>
  </div>
  @endsection

@endsection
@push ('page-scripts')
@include('pinjam_buku.js.pinjam_buku-js')
@endpush