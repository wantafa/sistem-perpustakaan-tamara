<div class="row">
    <div class="col-md-6">
        <div class="form-group">
        <input type="hidden" value="{{ $pinjam_buku->id }}" id="id_data"/>
            <label>Status</label>
            <select name="status" class="form-control border-primary status">
                <option value="">Pilih Status</option>
                <option value="Kembali">Kembali</option>
                <option value="Hilang atau Rusak">Hilang atau Rusak</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Tanggal Kembali</label>
            <input id="tgl_kmbl_real" name="tgl_kmbl_real" placeholder="Masukkan Denda" type="text"
                class="form-control border-primary" value="{{$pinjam_buku->tgl_kmbl_real}}" required>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group denda">
            <label>Denda</label>
            <input id="denda" name="denda" placeholder="Masukkan Denda" type="text"
                class="form-control border-primary" value="{{$pinjam_buku->denda}}" required>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Keterangan</label>
            <input id="keterangan" name="keterangan" placeholder="Masukkan Keterangan" type="text"
                class="form-control border-primary" value="{{$pinjam_buku->keterangan}}">
        </div>
    </div>
    <input type="hidden" class="tgl_kembali" value="{{ $pinjam_buku->tgl_kembali }}">
    <input type="hidden" class="tgl_kembali_real" value="{{ \Carbon\Carbon::now()->format('y-m-d') }}">


<script>

$('.denda').hide();

var tgl_kembali = new Date($('.tgl_kembali').val());

var tgl_kembali_real = new Date();

if (tgl_kembali_real > tgl_kembali.getTime()) {
    
    $('.denda').show();
    
}

</script>