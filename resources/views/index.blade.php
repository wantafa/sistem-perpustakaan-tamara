@extends ('layouts.master')
@section('title', 'Dashboard')
@section('content')
<div class="section-body">
    <!-- Main Content -->
      <div class="section-header">
        <h1>Dashboard</h1>
      </div>
      <div class="alert alert-info alert-has-icon">
        <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
        <div class="alert-body">
          <div class="alert-title">Hallo, {{ Auth::user()->name }}</div>
          Selamat Datang Di Sistem Informasi Perpustakaan Pada SMA Negeri 14 Kabupaten Tangerang!
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-success">
              <i class="fas fa-book-open"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Data Pinjam</h4>
              </div>
              <div class="card-body">
                {{ $semua_pinjam }}
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-primary">
              <i class="fas fa-chevron-right"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Sedang Pinjam</h4>
              </div>
              <div class="card-body">
                {{ $sedang_pinjam }}
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-info">
              <i class="fas fa-book"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Buku</h4>
              </div>
              <div class="card-body">
                {{ $buku }}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-6 col-sm-6">
          <div class="card">
            <div class="card-header">
              <h4>Buku Sedang Pinjam</h4>
            </div>
            <div class="card-header">
              <div class="table-responsive">
                <table class="table table-hover table-striped" id="table-1">
                  <thead>                                 
                    <tr class="table-info">
                      <th>Kode Pinjam</th>
                      <th>Buku</th>
                      <th>Peminjam</th>
                      <th>Tanggal Pinjam</th>
                      <th>Tanggal Kembali</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>             
                    @foreach ($buku_pinjam as $item)
                    <tr>
                      <td>{{ $item->kd_pinjam}}</td>
                      <td>{{ $item->buku->judul}}</td>
                      <td>{{ $item->user->name}}</td>
                      <td>{{ tgl_ind($item->tgl_pinjam)}}</td>
                      <td>{{ tgl_ind($item->tgl_kembali)}}</td>
                      <td><span class="badge badge-primary">{{ $item->status}} </span></td>
                      @if (Auth::user()->role == 'admin' || Auth::user()->role == 'petugas' )
                          
                      <td>
                      
                      <form action="{{ route('transaksi.update', $item->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('patch')
                        <button class="btn btn-icon btn-dark btn-sm" onclick="return confirm('Anda yakin Buku ini sudah kembali?')"> Sudah Kembali
                        </button>
                      </form>
                        </td>
                      @endif

                      </tr>
                    @endforeach                    
                  </tbody>
                </table>
              </div>
            </div>
            
          </div>
        </div>
        </div>
        <script src="https://demo.getstisla.com/assets/modules/chart.min.js"></script>

<script>
"use strict";

var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    
    datasets: [{
      label: 'Rating',
      data: [
        20, 15
      ],
      borderWidth: 2,
      backgroundColor: [
                '#E6E6FA',
                '#FFF0F5',
                '#ADD8E6',
                '#FFB6C1',
            ],
      borderColor: '#6777ef',
    }],
    labels: [
      "Cerita 1", "Cerita 2"
            ],
  },
  options: {
    responsive: true,
    legend: {
      position: 'botom',
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
        }
      }],
    },
  }
});

</script>
    @endsection


    @push ('page-scripts')
    
    
    @endpush
