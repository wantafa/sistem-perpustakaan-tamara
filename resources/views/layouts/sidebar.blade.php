<div class="main-sidebar">
    <aside id="sidebar-wrapper">
      <div class="sidebar-brand mt-2 mb-5">
        <img src="/assets/img/logo.png" width="110" height="110">
      </div>
      <div class="sidebar-brand sidebar-brand-sm">
        <a href="/">SMA 14</a>
      </div>
      <ul class="sidebar-menu">
          <li class="menu-header">Dashboard</li>
          <li class="{{ Request::is('dashboard') ? 'active' : '' }}"><a class="nav-link" href="/dashboard"><i class="fas fa-tachometer-alt"></i> <span>Dashboard</span></a></li>
          @if (Auth::user()->role == 'admin' || Auth::user()->role == 'kepala_sekolah')
          <li class="dropdown {{ Request::is('anggota', 'buku') ? 'active' : '' }}">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Master Data</span></a>
            <ul class="dropdown-menu">
              <li class="{{ Request::is('anggota') ? 'active' : '' }}"><a class="nav-link " href="{{ route('anggota') }}">Data Anggota</a></li>
              <li class="{{ Request::is('buku') ? 'active' : '' }}"><a class="nav-link" href="{{ route('buku')}}">Data Buku</a></li>
            </ul>
          </li>
          @endif
          <li class="{{ Request::is('transaksi') ? 'active' : '' }}"><a class="nav-link" href="{{ route('transaksi') }}"><i class="fas fa-book"></i> <span>Transaksi</span></a></li>
          @if (Auth::user()->role == 'admin' || Auth::user()->role == 'kepala_sekolah' )
          <li class="menu-header">Laporan</li>
          <li class="{{ Request::is('pinjam_buku/laporan') ? 'active' : '' }}"><a class="nav-link" href="{{ route('cetak')}}"><i class="fas fa-list"></i> <span>Laporan</span></a></li> 
          <li class="menu-header">Manajemen User</li>
          <li class="dropdown {{ Request::is('manajemen_user', 'role') ? 'active' : '' }}">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-users"></i><span>Manajemen User</span></a>
            
            <ul class="dropdown-menu">
              <li class="{{ Request::is('manajemen_user') ? 'active' : '' }}"><a class="nav-link" href="{{ route('manajemen_user.index')}}">USER</a></li>
            </ul>
          </li>
          @endif
    </aside>
  </div>