<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PinjamBuku extends Model
{
    protected $table = 'transaksi';
    protected $fillable = [
        'buku_id',
        'user_id',
        'kd_pinjam',
        'tgl_pinjam',
        'tgl_kembali',
        'tgl_kmbl_real',
        'status',
        'denda',
        'keterangan',
    ];

    public function buku()
    {
        return $this->belongsTo(Buku::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
