<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'buku';
    protected $fillable = [
        'judul',
        'penerbit',
        'pengarang',
        'jml_buku',
        'tahun_terbit',
        'deskripsi',
        'buku',
        'lokasi',
        'foto',
    ];

    public function pinjam_buku()
    {
        return $this->hasMany(PinjamBuku::class);
    }
}
