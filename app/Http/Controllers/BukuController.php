<?php

namespace App\Http\Controllers;

use Auth;
use Alert;
use App\Buku;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;

class BukuController extends Controller
{
    public function index() {
        if(Auth::user()->role == 'user') {
            Alert::error('Maaf..', 'Anda dilarang masuk ke area ini.');
            return redirect()->to('/dashboard');
        }
        

        $buku = Buku::all();
        $no = 1;
        return view('buku.index', compact('buku', 'no'));
    }

    public function create() {
        //
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|min:5',
            'penerbit' => 'required',
            'pengarang' => 'required',
            'tahun_terbit' => 'required',
            'deskripsi' => 'required',
            'lokasi' => 'required',
            'foto' => 'required|max:5000',
        ]);

        $data = $request->all();
        
        if ($request->hasFile('foto')) {
            $foto = 'uploads/foto/'. time() . '.' . $request->file('foto')->getClientOriginalName(); 
            $request->file('foto')->move('uploads/foto', $foto);
            
            $data['foto'] = $foto;
        }

        Buku::create($data);
        
        alert()->success('Berhasil.','Data Buku ditambahkan!');
        return redirect('buku');
    }

    public function show($id)
    {
        $buku = Buku::find($id);
        if(Auth::user()->role == 'user') {
            Alert::error('Maaf..', 'Anda dilarang masuk ke area ini.');
            return redirect()->to('/dashboard');
        }
        return view('buku.show', compact('buku'));
    }

     public function edit($id)
     {
         $buku = Buku::find($id);
        if(Auth::user()->role == 'user') {
            Alert::info('Oopss..', 'Anda dilarang masuk ke area ini.');
            return redirect()->to('/dashboard');
        }
       return view('buku.edit', compact('buku'));
     }
     

    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|min:5',
            'penerbit' => 'required',
            'pengarang' => 'required',
            'tahun_terbit' => 'required',
            'deskripsi' => 'required',
            'lokasi' => 'required',
            'foto' => 'max:5000',
        ]);

        $buku = Buku::find($id);
        $data = $request->all();
        $foto = $buku->foto;

        if ($request->hasFile('foto')) {
            File::delete($buku->foto);

            $foto = 'uploads/foto/'. time() . '_' . $request->file('foto')->getClientOriginalName();
            $request->file('foto')->move('uploads/foto', $foto);
        }
        $data = $request->except('_method', '_token');
        $data['foto'] = $foto;

        Buku::where('id',$id)->update($data);
    }

     public function destroy($id)
    {
        $item = Buku::findOrFail($id);
        $item->delete();
        return redirect()->route('buku');
    }
}
