<?php

namespace App\Http\Controllers;

use App\Buku;
use App\PinjamBuku;
use Auth;
use Illuminate\Http\Request;
Use Alert;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        toast('Login Berhasil', 'success')->position('center')->width('300px')->timerProgressBar()->background('#DAEAEC');
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->role == 'admin' || Auth::user()->role == 'petugas') {
            
            $semua_pinjam = PinjamBuku::count();
            $buku = Buku::count();
            $sedang_pinjam = PinjamBuku::where('status', 'Pinjam')->count();
            $buku_pinjam = PinjamBuku::where('status', 'Pinjam')->get();
        }

            $semua_pinjam = PinjamBuku::where('user_id', Auth::user()->id)->count();
            $buku = Buku::count();
            $sedang_pinjam = PinjamBuku::where('status', 'Pinjam')->where('user_id', Auth::user()->id)->count();
            $buku_pinjam = PinjamBuku::where('status', 'Pinjam')->where('user_id', Auth::user()->id)->get();

        return view('index', compact('buku_pinjam','semua_pinjam','sedang_pinjam','buku'));
    }
}
