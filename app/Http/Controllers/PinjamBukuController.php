<?php

namespace App\Http\Controllers;

use Auth;
use App\Buku;
use App\User;
use App\PinjamBuku;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class PinjamBukuController extends Controller
{
    public function index() {

        if(Auth::user()->role == 'admin' || Auth::user()->role == 'petugas') {

            
            $pinjam_buku = PinjamBuku::all();
        }
        
        $pinjam_buku = PinjamBuku::where('user_id', Auth::user()->id)->get();
        
        $buku = Buku::where('jml_buku', '>', 0)->get();
        $no = 1;
        return view('pinjam_buku.index', compact('pinjam_buku', 'buku', 'no'));
    }

    public function create() {
        //
    }

    public function store(Request $request)
    {
        $request->validate([
            'tgl_pinjam' => 'required',
            'tgl_kembali' => 'required',
            'keterangan' => 'required',
        ]);

        $data = $request->all();
        $data['status'] = 'Pinjam';
        $data['user_id'] = Auth::user()->id;
        $pinjam_buku = PinjamBuku::create($data);
        
        Buku::where('id', $pinjam_buku->buku_id)->update(['jml_buku' => $pinjam_buku->buku->jml_buku - 1]);

        alert()->success('Berhasil.','Pinjam Buku ditambahkan!');
        return redirect('transaksi');
    }

    public function view_proses($id)
    {
        $pinjam_buku = PinjamBuku::find($id);
        return view('pinjam_buku.proses', compact('pinjam_buku'));
    }
    
    public function proses_update(Request $request, $id)
    {
        $pinjam_buku = PinjamBuku::find($id);
        
        PinjamBuku::where('id', $pinjam_buku->id)->update([
            'status' => $request->status,
            'tgl_kmbl_real' => $request->tgl_kmbl_real,
            'denda' => $request->denda,
        ]
        );
        // return view('pinjam_buku.show', compact('pinjam_buku'));
    }

     public function edit($id)
     {
         $pinjam_buku = PinjamBuku::find($id);
         
        if(Auth::user()->role == 'user') {
            Alert::info('Oopss..', 'Anda dilarang masuk ke area ini.');
            return redirect()->to('/dashboard');
        }
        return view('pinjam_buku.edit', compact('pinjam_buku'));
     }
     

    public function update(Request $request, $id)
    {

        $pinjam_buku = PinjamBuku::find($id);
        
        $pinjam_buku->update(['status' => 'Kembali']);

        Buku::where('id', $pinjam_buku->buku_id)->update(['jml_buku' => $pinjam_buku->buku->jml_buku + 1]);

        alert()->success('Berhasil.','Buku Sudah Kembali!');
        return back();
    }

     public function destroy($id)
    {
        $item = PinjamBuku::findOrFail($id);
        $item->delete();
        return redirect()->route('transaksi');
    }

    public function cetakForm()
    {
      if(Auth::user()->role == 'user') {
        Alert::error('Maaf..', 'Anda dilarang masuk ke area ini.');
        return redirect()->to('/dashboard');
      }
        $pinjam_buku = PinjamBuku::all();
        $pinjam  = PinjamBuku::where('status','Pinjam')->get();
        $kembali  = PinjamBuku::where('status','Kembali')->get();
      return view('pinjam_buku.cetak-form', compact('pinjam_buku','pinjam','kembali'));
    }

    public function cetakpinjam($tglawal, $tglakhir)
    {
        $pinjam = PinjamBuku::whereBetween('tgl_pinjam', [$tglawal, $tglakhir])->where('status','Pinjam')->latest()->get();

        $buku_terbanyak = PinjamBuku::select('buku_id', DB::raw('count(*) as total'))->groupBy('buku_id')->orderBy('total', 'desc')->get();
        $no = 1;

      return view('pinjam_buku.cetak-pinjam', compact('pinjam','no','buku_terbanyak'));
    }
    public function cetakkembali($tglawal, $tglakhir)
    {
        $kembali = PinjamBuku::whereBetween('tgl_pinjam', [$tglawal, $tglakhir])->where('status','Kembali')->latest()->get();
        
        $buku_terbanyak = PinjamBuku::select('buku_id', DB::raw('count(*) as total'))->groupBy('buku_id')->orderBy('total', 'desc')->get();
        
        $no = 1;
        return view('pinjam_buku.cetak-kembali', compact('kembali','no','buku_terbanyak'));
    }
    
}
